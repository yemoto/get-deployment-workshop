# AWS 3K Reference Architecture with cloud managed services

:exclamation: This is a continuation of the AWS 3K deployment workshop with GET. It assumes you have a working deployment of GitLab that was deployed with GET. If you don't have this, create a [an issue](https://gitlab.com/gitlab-org/professional-services-automation/tools/implementation/GET-deployment-workshop/-/issues/new?issuable_template=aws-3k-deployment-workshop.md) complete the prerequesite steps.

> This workshop will focus on replacing some of the stateful components that were originally deployed using VMs, now with cloud managed services. 

## Replacing Omnibus Components for AWS managed Services

We are going to be replacing Postgres/Patroni and Pgbouncer for RDS. Redis/Sentinel for Elasticache. And the Internal Load Balancer that sits in front of praefect for NLB on ELB. We are going to be doing this step-by-step. So, first, let's use GET to provision these components for us.

1. [ ] On the instance terminal edit `gitlab-environment-toolkit/terraform/environments/3k/variables.tf` including Elasticache and RDS passwords:

```
variable "elasticache_redis_password" {
    type = string
}

variable "rds_postgres_password" {
    type = string
}
```

2. [ ] Create `gitlab-environment-toolkit/terraform/environments/3k/outputs.tf` files to access the internal module outputs to retrive Elasticache, RDS and ILB details:

```
output "rds_postgres_connection" {
    value = try(module.gitlab_ref_arch_aws.rds_postgres_connection, [])
}

output "elasticache_redis_persistent_connection" {
    value = try(module.gitlab_ref_arch_aws.elasticache_redis_connection, [])
}

output "gitlab_internal_load_balancer_dns" {
  value = try(module.gitlab_ref_arch_aws.elb_internal_host, [])
}
```
3. [ ] On the instance terminal edit `gitlab-environment-toolkit/terraform/environments/3k/environment.tf` adding GET required entries to provision [Elasticache](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_advanced_services.md#aws-elasticache) and [RDS](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_advanced_services.md#aws-rds) and deploy and Internal Load Balancer on ELB.

```
# ILB 
elb_internal_create = true

# RDS
rds_postgres_instance_type = "m5.2xlarge"
rds_postgres_password = var.rds_postgres_password

# Elasticcache
elasticache_redis_node_count = 2
elasticache_redis_instance_type = "m5.large"
elasticache_redis_password = var.elasticache_redis_password
```

4. [ ] Before running Terraform we need to export environment variables `elasticache_redis_password` and `rds_postgres_password` we can use the same password already set on `$GITLAB_PASSWORD` 

```
export TF_VAR_rds_postgres_password=$GITLAB_PASSWORD
export TF_VAR_elasticache_redis_password=$GITLAB_PASSWORD
```

5. [ ] Run a Toolkit Docker Container adding the new variables. The command should look like following:

```
docker run -it  \
-v /home/ec2-user/gitlab-environment-toolkit/keys:/gitlab-environment-toolkit/keys \
-v /home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k:/gitlab-environment-toolkit/ansible/environments/3k \
-v /home/ec2-user/gitlab-environment-toolkit/terraform/environments/3k:/gitlab-environment-toolkit/terraform/environments/3k \
-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
-e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
-e GITLAB_PASSWORD=$GITLAB_PASSWORD \
-e TF_VAR_rds_postgres_password=$GITLAB_PASSWORD \
-e TF_VAR_elasticache_redis_password=$GITLAB_PASSWORD \
registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest
```

6. [ ] Inside the container run the commands below:
    - [ ] Run `cd /gitlab-environment-toolkit/terraform/environments/3k/`
    - [ ] Run `terraform apply`. This should add 20 new resources, and usually take 8-10 minutes
    - [ ] Run `terraform output`. Copy the output and poste as a comment on this issue

Now let's configure GitLab to use the RDS, Elasticache, and the ELB. We going to do that in three steps:
    1. Let's configure GitLab to use the new resources;
    1. Check that the application is still working
    1. Then remove unnecessary components;

7. [ ] In the instance edit `/home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k/inventory/vars.yml` adding the following lines:
    - [ ] Replace `<internal_lb_host>`, `<redis_host>` and `<postgres_host>` respectively for the terraform outputs: `gitlab_internal_load_balancer_dns`, `elasticache_redis_address` and `rds_host`.

```
postgres_external: true
internal_lb_host: "<internal_lb_host>"
redis_host: "<redis_host>"
postgres_host: "<postgres_host>"
```
Something similar to the following:
```
postgres_external: true
internal_lb_host: "gitlab-afonseca-3k-paris-int-4353bac0a6a67bb2.elb.eu-west-3.amazonaws.com"
redis_host: "master.gitlab-afonseca-3k-paris-redis.jrodnd.euw3.cache.amazonaws.com"
postgres_host: "gitlab-afonseca-3k-paris-rds.coy2o6w62emn.eu-west-3.rds.amazonaws.com"
```

8. [ ] Inside the container let's run the Ansible scripts
   - [ ] Run `cd /gitlab-environment-toolkit/ansible`
   - [ ] Test connection with hosts `ansible all -m ping -i environments/3k/inventory --list-hosts`
   - [ ] Run `ansible-playbook -i environments/3k/inventory/ playbooks/all.yml`
       - [ ] Manual fix of the task `Get Omnibus Postgres Primary` https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/merge_requests/555 when running GET older than `2.0.1` 
   - [ ] Once the process is complete exit the container with `exit`

9. [ ] Access a rails node and confirm that is using the RDS and Elasticache in the terminal run:
    - [ ] run `ssh -i /home/ec2-user/gitlab-environment-toolkit/keys/id_rsa ubuntu@your_rails_public_dns`
    - [ ] run `sudo vim /etc/gitlab/gitlab.rb`
    - [ ] Check the configurations: `gitlab_rails['db_host']`, `gitlab_rails['redis_host']` and `gitaly_address`. They must be pointing for `postgres_host`, `redis_host` and `internal_lb_host` provide on step `7`

10. [ ] Test the GitLab application using `http://<your_elastic_ip>` the same information provided on `external_url` in the `vars.yml`. With user `root` and the password as set in the variable `GITLAB_PASSWORD`. If everything is working as expected move to the next step.

11. [ ] Remove the Redis, Postgres, Pgbouncer and HAProxy Internal editing `/home/ec2-user/gitlab-environment-toolkit/terraform/environments/3k/environment.tf` and commenting/removing the lines below:
```
    # redis_node_count = 3
    # redis_instance_type = "m5.large"

    # postgres_node_count = 3
    # postgres_instance_type = "m5.large"

    # pgbouncer_node_count = 3
    # pgbouncer_instance_type = "c5.large"

    # haproxy_internal_node_count = 1
    # haproxy_internal_instance_type = "c5.large"
```
12. [ ] Run the Toolkit's container running the command below:
```
docker run -it  \
-v /home/ec2-user/gitlab-environment-toolkit/keys:/gitlab-environment-toolkit/keys \
-v /home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k:/gitlab-environment-toolkit/ansible/environments/3k \
-v /home/ec2-user/gitlab-environment-toolkit/terraform/environments/3k:/gitlab-environment-toolkit/terraform/environments/3k \
-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
-e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
-e GITLAB_PASSWORD=$GITLAB_PASSWORD \
-e TF_VAR_rds_postgres_password=$GITLAB_PASSWORD \
-e TF_VAR_elasticache_redis_password=$GITLAB_PASSWORD \
registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest
```
13. [ ] Inside the container run the commands that follow:
     - [ ] `cd /gitlab-environment-toolkit/terraform/environments/3k`
     - [ ] `terraform apply` this should destroy 19 resources

This should not affect the application, since the components are not being used anymore. Once you are done with the environment dont fortget to remove all the deployed resources on AWS, going over the next and final step.
    
14. [ ] Inside container the container once you are done with your environment don't forget to tear it down;
     - [ ] `cd /gitlab-environment-toolkit/terraform/environments/3k`
     - [ ] `terraform destroy`
     - [ ] Terminate GET instance manually through AWS console
